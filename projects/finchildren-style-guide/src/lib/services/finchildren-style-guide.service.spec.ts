import { TestBed } from '@angular/core/testing';

import { FinChildrenStyleGuide } from './finchildren-style-guide.service';

describe('FinChildrenStyleGuide', () => {
  let service: FinChildrenStyleGuide;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FinChildrenStyleGuide);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
