import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'finchildren-style-guide',
  templateUrl: './finchildren-style-guide.component.html',
  styleUrls: ['./finchildren-style-guide.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FinChildrenStyleGuideComponent implements OnInit {

  @Input() type: string = 'basic';

  constructor() { }

  ngOnInit(): void {

  }

}
