import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FinChildrenStyleGuideComponent } from './finchildren-style-guide.component';
import { MatButtonFnModule } from '@finchildren/mat-button-fn';


@NgModule({
  declarations: [
    FinChildrenStyleGuideComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatButtonFnModule
  ],
  exports: [
    FinChildrenStyleGuideComponent,
    MatButtonFnModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FinChildrenStyleGuideModule { }
