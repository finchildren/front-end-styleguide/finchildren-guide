import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinChildrenStyleGuideComponent } from './finchildren-style-guide.component';

describe('FinChildrenStyleGuideComponent', () => {
  let component: FinChildrenStyleGuideComponent;
  let fixture: ComponentFixture<FinChildrenStyleGuideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinChildrenStyleGuideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinChildrenStyleGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
