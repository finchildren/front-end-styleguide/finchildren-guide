# finchildren-style-guide

Componente de button utilizando o angular material

## Instalação projeto local

- Retirar do package.json a referência da lib (EM CASO DE CLONE INICIAL)

`@finchildren/finchildren-style-guide": "file:dist/finchildren-style-guide,`

- Rodar o comando local para instalar as libs

`npm i`

- Na sequência rodar o seguinte comando para instalar a lib

`npm run build:local`

- Incluir novamente no package.json a referência da lib (EM CASO DE CLONE INICIAL)

`@finchildren/finchildren-style-guide": "file:dist/finchildren-style-guide,`


## Inicialização do projeto

- Rodar o seguinte comando

`npm start`


## Publicação do projeto npm

### Apenas na primeira vez

- Alterar no package.json para "private": false,
- Ir até a pasta da lib projects\finchildren-style-guide
- Tornar o projeto local publico, rodando o seguinte comando (APENAS NA PRIMEIRA VEZ QUE FOR PUBLICAR)

`npm publish --access=public`

-- Retornar a raiz do projeto e rodar o comando npm run build:publish
### Quando não for a primeira vez

- Rodar o seguinte comando:

`npm run build:publish`
